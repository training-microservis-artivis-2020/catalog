FROM artifactory.pegadaian.co.id:8084/openjdk:8u262-jre-slim
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]